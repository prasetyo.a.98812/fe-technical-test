import { render, screen } from "@testing-library/react";
import App from "./App";
import { Provider } from "react-redux";
import store from "./store";
const TestComponent = (
  <Provider store={store}>
    <App />
  </Provider>
);
test("renders learn react link", () => {
  render(<TestComponent />);
  const linkElement = screen.getByText(/Sign in to start your session/i);
  expect(linkElement).toBeInTheDocument();
});
