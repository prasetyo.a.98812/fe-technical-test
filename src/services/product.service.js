class ProductService {
  create(payload) {
    const newPro = new Promise((resolve, reject) => {
      try {
        return resolve({ data: payload, message: "Added" });
      } catch (err) {
        return reject(new Error({ message: "Error" }));
      }
    });

    return newPro;
  }
  list() {
    const newPro = new Promise((resolve, reject) => {
      try {
        return resolve();
      } catch (err) {
        return reject(new Error({ message: "Error" }));
      }
    });

    return newPro;
  }
  edit(payload) {
    const newPro = new Promise((resolve, reject) => {
      try {
        return resolve({ data: payload, message: "Updated" });
      } catch (err) {
        return reject(new Error({ message: "Error" }));
      }
    });

    return newPro;
  }

  delete(payload) {
    const newPro = new Promise((resolve, reject) => {
      try {
        return resolve(payload);
      } catch (err) {
        return reject(new Error({ message: "Error" }));
      }
    });

    return newPro;
  }
}

export default new ProductService();
