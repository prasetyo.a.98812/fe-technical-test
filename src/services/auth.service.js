class AuthService {
  login(username, password) {
    const newPro = new Promise((resolve, reject) => {
      try {
        const verifyToken = {
          role: username,
        };
        localStorage.setItem("user", JSON.stringify(verifyToken));
        return resolve({ data: verifyToken });
      } catch (err) {
        return reject(new Error("error"));
      }
    });

    return newPro;
  }
  logout() {
    localStorage.clear();
  }
}

export default new AuthService();
