import dummyDB from "../database/dummy.json";

class MenuService {
  getMenu(role) {
    const newPro = new Promise((resolve, reject) => {
      try {
        const roleNow = dummyDB.find((db) => db.role == role);
        localStorage.setItem("menu", JSON.stringify(roleNow.menu));

        return resolve(roleNow);
      } catch (err) {
        return reject(new Error({ message: "Error" }));
      }
    });

    return newPro;
  }
}

export default new MenuService();
