class WarrantyService {
  approve(payload) {
    const newPro = new Promise((resolve, reject) => {
      try {
        return resolve({ data: payload, message: "Approved" });
      } catch (err) {
        return reject(new Error({ message: "Error" }));
      }
    });

    return newPro;
  }
  list() {
    const newPro = new Promise((resolve, reject) => {
      try {
        return resolve();
      } catch (err) {
        return reject(new Error({ message: "Error" }));
      }
    });

    return newPro;
  }
  rejected(payload) {
    const newPro = new Promise((resolve, reject) => {
      try {
        return resolve({ data: payload, message: "Rejected" });
      } catch (err) {
        return reject(new Error({ message: "Error" }));
      }
    });

    return newPro;
  }
}

export default new WarrantyService();
