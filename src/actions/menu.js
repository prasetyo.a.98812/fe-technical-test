import {
  SET_ACTIVE_MENU,
  CLEAR_ACTIVE_MENU,
  SET_ACTIVE_DASHBOARD,
  SET_MESSAGE,
  GET_MENU_SUCCESS,
  GET_MENU_FAIL,
} from "./types";
import MenuService from "../services/menu.service";

export const setActiveMenu = (menu) => (dispatch) => {
  localStorage.setItem("active_menu", JSON.stringify(menu));
  return {
    type: SET_ACTIVE_MENU,
    payload: menu,
  };
};

export const setActiveDashboard = () => ({
  type: SET_ACTIVE_DASHBOARD,
});

export const clearActiveMenu = () => ({
  type: CLEAR_ACTIVE_MENU,
});

export const getMenu = (role) => (dispatch) => {
  return MenuService.getMenu(role).then(
    (data) => {
      console.log("data>>", data);
      dispatch({
        type: GET_MENU_SUCCESS,
        payload: data,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: GET_MENU_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};
