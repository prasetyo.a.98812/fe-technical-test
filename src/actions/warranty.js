import {
  APPROVE_WARRANTY_SUCCESS,
  APPROVE_WARRANTY_FAIL,
  READ_WARRANTY_SUCCESS,
  READ_WARRANTY_FAIL,
  REJECT_WARRANTY_SUCCESS,
  REJECT_WARRANTY_FAIL,
  SET_MESSAGE,
} from "./types";
import WarrantyService from "../services/warranty.service";

export const approve = (payload) => (dispatch) => {
  return WarrantyService.approve(payload).then(
    (response) => {
      dispatch({
        type: APPROVE_WARRANTY_SUCCESS,
        payload: response.data,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: response.message,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response && error.response.data && error.response.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: APPROVE_WARRANTY_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};

export const reject = (payload) => (dispatch) => {
  return WarrantyService.reject(payload).then(
    (response) => {
      dispatch({
        type: REJECT_WARRANTY_SUCCESS,
        payload: response.data,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: response.message,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response && error.response.data && error.response.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: REJECT_WARRANTY_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};
export const list = () => (dispatch) => {
  return WarrantyService.list().then(
    () => {
      dispatch({
        type: READ_WARRANTY_SUCCESS,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response && error.response.data && error.response.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: READ_WARRANTY_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};
