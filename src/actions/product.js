import {
  CREATE_PRODUCT_SUCCESS,
  CREATE_PRODUCT_FAIL,
  READ_PRODUCT_SUCCESS,
  READ_PRODUCT_FAIL,
  EDIT_PRODUCT_SUCCESS,
  EDIT_PRODUCT_FAIL,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAIL,
  SET_MESSAGE,
} from "./types";
import ProductService from "../services/product.service";

export const addProduct = (payload) => (dispatch) => {
  return ProductService.create(payload).then(
    (response) => {
      dispatch({
        type: CREATE_PRODUCT_SUCCESS,
        payload: response.data,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: response.message,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response && error.response.data && error.response.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: CREATE_PRODUCT_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};

export const editProduct = (payload) => (dispatch) => {
  return ProductService.edit(payload).then(
    (response) => {
      dispatch({
        type: EDIT_PRODUCT_SUCCESS,
        payload: response.data,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: response.message,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response && error.response.data && error.response.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: EDIT_PRODUCT_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};
export const deleteProduct = (payload) => (dispatch) => {
  return ProductService.delete(payload).then(
    (response) => {
      dispatch({
        type: DELETE_PRODUCT_SUCCESS,
        payload: response.data,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: response.message,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response && error.response.data && error.response.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: DELETE_PRODUCT_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};

export const readProduct = () => (dispatch) => {
  return ProductService.list().then(
    () => {
      dispatch({
        type: READ_PRODUCT_SUCCESS,
      });

      return Promise.resolve();
    },
    (error) => {
      const message =
        (error.response && error.response.data && error.response.message) ||
        error.message ||
        error.toString();

      dispatch({
        type: READ_PRODUCT_FAIL,
      });

      dispatch({
        type: SET_MESSAGE,
        payload: message,
      });

      return Promise.reject();
    }
  );
};
