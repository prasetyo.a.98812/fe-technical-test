import {
  APPROVE_WARRANTY_SUCCESS,
  APPROVE_WARRANTY_FAIL,
  READ_WARRANTY_SUCCESS,
  READ_WARRANTY_FAIL,
  REJECT_WARRANTY_SUCCESS,
  REJECT_WARRANTY_FAIL,
} from "../actions/types";

const initialState = {
  warranty: [
    {
      id: 1,
      name: "Product 1",
      code: "PC01",
      customer: "Customer A",
      status: "Submitted",
    },
    {
      id: 2,
      name: "Product 2",
      code: "PC01",
      customer: "Customer B",
      status: "Submitted",
    },
    {
      id: 3,
      name: "Product 3",
      code: "PC01",
      customer: "Customer C",
      status: "Submitted",
    },
  ],
};

export default function warranty(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case APPROVE_WARRANTY_SUCCESS:
      return {
        ...state,
        warranty: state.warranty.map((content, i) =>
          content.id === payload.id
            ? {
                ...content,
                status: "APPROVED",
              }
            : content
        ),
      };
    case APPROVE_WARRANTY_FAIL:
      return {
        ...state,
      };

    case REJECT_WARRANTY_SUCCESS:
      return {
        ...state,
        warranty: state.warranty.map((content, i) =>
          content.id === payload.id
            ? {
                ...content,
                status: "REJECTED",
              }
            : content
        ),
      };
    case REJECT_WARRANTY_FAIL:
      return {
        ...state,
      };
    case READ_WARRANTY_SUCCESS:
      return {
        ...state,
      };
    case READ_WARRANTY_FAIL:
      return {
        ...state,
        read_warranty: null,
      };

    default:
      return state;
  }
}
