import { combineReducers } from "redux";
import auth from "./auth";
import message from "./message";
import menu from "./menu";
import department from "./department";
import product from "./product";
import warranty from "./warranty";

export default combineReducers({
  auth,
  message,
  menu,
  department,
  product,
  warranty,
});
