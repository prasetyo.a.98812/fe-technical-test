import {
  CREATE_PRODUCT_SUCCESS,
  CREATE_PRODUCT_FAIL,
  READ_PRODUCT_SUCCESS,
  READ_PRODUCT_FAIL,
  EDIT_PRODUCT_SUCCESS,
  EDIT_PRODUCT_FAIL,
  DELETE_PRODUCT_SUCCESS,
  DELETE_PRODUCT_FAIL,
} from "../actions/types";

const initialState = {
  product: [
    { id: 1, name: "Product 1", code: "PC01" },
    { id: 2, name: "Product 2", code: "PC01" },
    { id: 3, name: "Product 3", code: "PC01" },
  ],
};

export default function product(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case CREATE_PRODUCT_SUCCESS:
      return {
        ...state,
        product: state.product.concat(payload),
      };
    case CREATE_PRODUCT_FAIL:
      return {
        ...state,
      };

    case EDIT_PRODUCT_SUCCESS:
      return {
        ...state,
        product: state.product.map((content, i) =>
          content.id === payload.id
            ? {
                ...content,
                name: payload.name,
                code: payload.code,
              }
            : content
        ),
      };
    case EDIT_PRODUCT_FAIL:
      return {
        ...state,
      };
    case READ_PRODUCT_SUCCESS:
      return {
        ...state,
      };
    case READ_PRODUCT_FAIL:
      return {
        ...state,
        read_product: null,
      };
    case DELETE_PRODUCT_SUCCESS:
      return {
        ...state,
        product: state.product.filter((item) => item.id !== action.payload),
      };
    case DELETE_PRODUCT_FAIL:
      return {
        ...state,
      };

    default:
      return state;
  }
}
