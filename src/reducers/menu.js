import {
  SET_ACTIVE_MENU,
  CLEAR_ACTIVE_MENU,
  SET_ACTIVE_DASHBOARD,
  GET_MENU_SUCCESS,
  GET_MENU_FAIL,
} from "../actions/types";

const menuStorage = JSON.parse(localStorage.getItem("menu"));
const activeMenu = JSON.parse(localStorage.getItem("active_menu"));
const initialMenu = menuStorage ? menuStorage : null;
const initialActiveMenu = activeMenu ? activeMenu : null;
const initialState = {
  isDashboardOpen: true,
  active_menu: initialActiveMenu,
  menu: initialMenu,
  read_menu: null,
  read_submenu: null,
};

export default function menu(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case SET_ACTIVE_MENU:
      return {
        ...state,
        active_menu: payload,
        isDashboardOpen: false,
      };

    case SET_ACTIVE_DASHBOARD:
      return {
        ...state,
        active_menu: "",
        isDashboardOpen: true,
      };
    case CLEAR_ACTIVE_MENU:
      return {
        ...state,
        active_menu: "",
      };
    case GET_MENU_SUCCESS:
      return {
        ...state,
        menu: payload,
      };

    case GET_MENU_FAIL:
      return {
        ...state,
        menu: null,
      };
    default:
      return state;
  }
}
