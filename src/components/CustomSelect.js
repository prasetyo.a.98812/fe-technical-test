import React, { Component } from "react";
import Select from 'react-select';class CustomSelect extends React.Component {
  render() {
    const {
      isValid,
      defaultValue
    } = this.props
    
    const customStyles = {
      control: (base, state) => ({
        ...base,
        borderColor: state.isFocused  ?
          '#ddd' : isValid ?
          '#ddd' : 'red',
        // overwrittes hover style
        '&:hover': {
          borderColor: state.isFocused ?
            '#ddd' : isValid ?
            '#ddd' : 'green'
        }
      })
    }
    return <Select styles={ customStyles } defaultValue={defaultValue} {...this.props} />
  }
}
export default CustomSelect;