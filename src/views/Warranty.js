import React, { Component } from "react";
import Footer from "../components/templates/Footer";
import Header from "../components/templates/Header";
import SideMenu from "../components/templates/SideMenu";
import { connect } from "react-redux";
import { approve, list, reject } from "../actions/warranty";
import { clearMessage } from "../actions/message";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import Swal from "sweetalert2";
import RemoteTable from "../components/RemoteTable";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-toastify/dist/ReactToastify.min.css";
import Modal from "react-bootstrap/Modal";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

class Warranty extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      code: null,
      name: null,
      customer: null,
      warranty: [],
      showModal: false,
      columns: [
        {
          dataField: "name",
          text: "Name",
          sort: true,
        },
        {
          dataField: "code",
          text: "Code",
          sort: true,
        },
        {
          dataField: "customer",
          text: "Customer",
          sort: true,
        },
        {
          dataField: "status",
          text: "Status",
          sort: true,
        },
        {
          text: "Action",
          dataField: "",
          formatter: this.GetActionFormat,
          classes: "text-center",
          headerStyle: { width: "20%" },
        },
      ],
      defaultSorted: [
        {
          dataField: "id",
          order: "asc",
        },
      ],
      cellEditProps: {},
    };
  }
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(list()).then((response) => {
      this.setState({ warranty: this.props.warranty });
    });
  }

  GetActionFormat = (cell, row) => {
    return (
      <div>
        <button
          type="button"
          className="btn btn-info btn-sm ml-2 ts-buttom"
          size="sm"
          data-id={row.id}
          onClick={() => this.detail(row.id, row.name, row.code, row.customer)}
        >
          <i className="fas fa-check"></i>
          Approval
        </button>
      </div>
    );
  };
  detail = (id, name, code, customer) => {
    this.setState(
      {
        id: id,
        name: name,
        code: code,
        customer: customer,
      },
      () => {
        this.toggle();
      }
    );
  };
  isEmptyObject = (obj) => {
    return obj == null || !Object.keys(obj).length;
  };
  toggle = () => {
    this.setState({
      showModal: !this.state.showModal,
    });
  };
  onModalHide = () => {
    this.setState({
      id: null,
      name: null,
      code: null,
      customer: null,
      showModal: false,
    });
  };
  approveData = (id) => {
    const { dispatch, history } = this.props;
    let collection = { id: id };
    if (collection) {
      Swal.fire({
        title: "Are you sure?",
        text: "Please check your entries",
        icon: "info",
        showCancelButton: true,
        confirmButtonText: "Yes, approve",
      }).then((result) => {
        if (result.value) {
          dispatch(approve(collection))
            .then((response) => {
              toast.success("Data has been approved successfully", {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                toastId: "customId-approve-product-success",
                onClose: () => dispatch(clearMessage()),
              });
              Swal.fire({
                title: "Information",
                icon: "success",
                text: "Data has been approved successfully",
                showConfirmButton: false,
                timer: 2000,
              });
              this.toggle();
              dispatch(list()).then((response) => {
                this.setState({ warranty: this.props.warranty });
              });
            })
            .catch((err) => {
              toast.error(this.props.message, {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                toastId: "customId-approve-product-failed",
                onClose: () => dispatch(clearMessage()),
              });
              this.setState({
                loading: false,
              });
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire("Cancelled", "Your data is safe :)", "error");
        }
      });
    } else {
      this.setState({
        loading: false,
      });
    }
  };
  rejectData = (id) => {
    const { dispatch, history } = this.props;
    let collection = { id: id };
    if (collection) {
      Swal.fire({
        title: "Are you sure?",
        text: "Please check your entries",
        icon: "info",
        showCancelButton: true,
        confirmButtonText: "Yes, reject",
      }).then((result) => {
        if (result.value) {
          dispatch(reject(collection))
            .then((response) => {
              toast.success("Data has been rejected successfully", {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                toastId: "customId-reject-product-success",
                onClose: () => dispatch(clearMessage()),
              });
              Swal.fire({
                title: "Information",
                icon: "success",
                text: "Data has been rejected successfully",
                showConfirmButton: false,
                timer: 2000,
              });
              this.toggle();
              dispatch(list()).then((response) => {
                this.setState({ warranty: this.props.warranty });
              });
            })
            .catch((err) => {
              toast.error(this.props.message, {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                toastId: "customId-reject-product-failed",
                onClose: () => dispatch(clearMessage()),
              });
              this.setState({
                loading: false,
              });
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire("Cancelled", "Your data is safe :)", "error");
        }
      });
    } else {
      this.setState({
        loading: false,
      });
    }
  };
  render() {
    let {
      columns,
      defaultSorted,
      cellEditProps,
      warranty,
      id,
      code,
      customer,
      showModal,
      name,
    } = this.state;
    return (
      <div className="sidebar-mini sidebar-collapse text-sm">
        <div className="wrapper">
          <ToastContainer />
          <Header />
          <SideMenu />
          <Modal
            show={showModal}
            size="xl"
            backdrop="static"
            onHide={this.onModalHide}
          >
            <form>
              <ModalHeader closeButton className="bg-info text-white">
                <ModalTitle>Detail </ModalTitle>
              </ModalHeader>
              <ModalBody>
                <Row>
                  <Col xs={12} md={6}>
                    <div className="form-group">
                      <label htmlFor="code">Product Code</label>
                      <input
                        type="text"
                        className="form-control form-control-sm"
                        name="code"
                        placeholder="Product Code"
                        defaultValue={code}
                        readOnly={true}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="name">Product Name</label>
                      <input
                        type="text"
                        className="form-control form-control-sm"
                        name="name"
                        placeholder="Product Name"
                        defaultValue={name}
                        readOnly={true}
                      />
                    </div>
                  </Col>
                  <Col xs={12} md={6}>
                    <div className="form-group">
                      <label htmlFor="customer">Customer</label>
                      <input
                        type="text"
                        className="form-control form-control-sm"
                        name="customer"
                        placeholder="Customer"
                        defaultValue={customer}
                        readOnly={true}
                      />
                    </div>
                  </Col>
                </Row>
              </ModalBody>
              <ModalFooter className="justify-content-between">
                <button
                  type="button"
                  className="btn btn-danger btn-sm ml-2 mb-2 ts-bottom float-left"
                  size="sm"
                  data-id={id}
                  onClick={() => this.rejectData(id)}
                >
                  <i className="fas fa-times"></i>
                  &nbsp;Reject
                </button>

                <button
                  type="button"
                  className="btn btn-success btn-sm ml-2 mb-2 ts-bottom float-right"
                  size="sm"
                  data-id={id}
                  onClick={() => this.approveData(id)}
                >
                  <i className="fas fa-check-square"></i>
                  &nbsp;Approve
                </button>
              </ModalFooter>
            </form>
          </Modal>
          <div className="content-wrapper">
            <div className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6"></div>
                  {/* /.col */}
                </div>
                {/* /.row */}
              </div>
              {/* /.container-fluid */}
            </div>

            <section className="content">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12 col-sm-12 col-12">
                    <div className="card card-info">
                      <div className="card-header ">
                        <h3 className="card-title">Warranty</h3>
                      </div>
                      <div className="card-body">
                        <RemoteTable
                          data={warranty}
                          columns={columns}
                          defaultSorted={defaultSorted}
                          cellEditProps={cellEditProps}
                        />
                      </div>
                    </div>
                  </div>
                  {/* /.col */}
                </div>
                {/* /.row */}
                {/* /.row */}
              </div>
              {/* /.container-fluid */}
            </section>
            {/* /.content */}
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { warranty } = state.warranty;
  const { message } = state.message;
  return {
    message,
    warranty,
  };
}

export default connect(mapStateToProps)(Warranty);
