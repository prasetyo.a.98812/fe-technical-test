import React, { Component } from "react";
import Footer from "../components/templates/Footer";
import Header from "../components/templates/Header";
import SideMenu from "../components/templates/SideMenu";
import { connect } from "react-redux";
import {
  addProduct,
  readProduct,
  editProduct,
  deleteProduct,
} from "../actions/product";
import { clearMessage } from "../actions/message";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import Modal from "../components/Modal";
import Swal from "sweetalert2";
import RemoteTable from "../components/RemoteTable";
import { textFilter } from "react-bootstrap-table2-filter";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-toastify/dist/ReactToastify.min.css";

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      name: "",
      code: "",
      displayModal: "none",
      titleModal: "Add Product",
      showModal: "show",
      displayModal: "none",
      modalVisible: false,
      collection: [],
      bodyModal: [
        { name: "name", placeholder: "Name", required: true, type: "text" },
        { name: "code", placeholder: "Code", required: true, type: "text" },
      ],
      product: [],
      columns: [
        {
          dataField: "name",
          text: "Name",
          filter: textFilter(),
          sort: true,
        },
        {
          dataField: "code",
          text: "Code",
          filter: textFilter(),
          sort: true,
        },
        {
          text: "Action",
          dataField: "",
          formatter: this.GetActionFormat,
          classes: "text-center",
          headerStyle: { width: "20%" },
        },
      ],
      defaultSorted: [
        {
          dataField: "name",
          order: "asc",
        },
      ],
      cellEditProps: {
        mode: "dbclick",
        beforeSaveCell: (oldValue, newValue, row, column) => {
          this.handleSubmitEdit(row.id, column.dataField, newValue);
        },
      },
    };
  }
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(readProduct()).then((response) => {
      this.setState({ product: this.props.product });
    });
  }

  handleSubmitEdit = (id, column, value) => {
    console.log(id, column, value);
    let collection = { id: id };
    collection[column] = value;
    const { dispatch, history } = this.props;
    if (collection) {
      Swal.fire({
        title: "Are you sure?",
        text: "Please check your entries",
        icon: "info",
        showCancelButton: true,
        confirmButtonText: "Yes, update entries",
      }).then((result) => {
        if (result.value) {
          dispatch(editProduct(collection))
            .then((response) => {
              toast.success("Data has been updated successfully", {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                toastId: "customId-edit-menu-success",
                onClose: () => dispatch(clearMessage()),
              });
              Swal.fire({
                title: "Information",
                icon: "success",
                text: "Data has been updated successfully",
                showConfirmButton: false,
                timer: 2000,
              });
            })
            .catch((err) => {
              toast.error(this.props.message, {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                toastId: "customId-edit-menu-failed",
                onClose: () => dispatch(clearMessage()),
              });
              this.setState({
                loading: false,
              });
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire("Cancelled", "Your data is safe :)", "error");
        }
      });
    } else {
      this.setState({
        loading: false,
      });
    }
  };
  GetActionFormat = (cell, row) => {
    return (
      <div>
        <button
          type="button"
          className="btn btn-danger btn-sm ml-2 ts-buttom"
          size="sm"
          data-id={row.id}
          onClick={() => this.deleteData(row.id)}
        >
          <i className="fas fa-trash"></i>
          Delete
        </button>
      </div>
    );
  };
  isEmptyObject = (obj) => {
    return obj == null || !Object.keys(obj).length;
  };
  onChangeValueParent = (any) => {
    if (!this.isEmptyObject(any)) {
      let object = any
        .filter(function (element) {
          return element !== undefined;
        })
        .reduce((obj, item) => {
          obj[item.key] = item.value;
          return obj;
        }, {});
      this.setState({ collection: object });
    }
  };
  deleteData = (id) => {
    const { dispatch, history } = this.props;
    let collection = { id: id };
    if (collection) {
      Swal.fire({
        title: "Are you sure?",
        text: "Please check your entries",
        icon: "info",
        showCancelButton: true,
        confirmButtonText: "Yes, delete entries",
      }).then((result) => {
        if (result.value) {
          dispatch(deleteProduct(collection))
            .then((response) => {
              toast.success("Data has been deleted successfully", {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                toastId: "customId-delete-product-success",
                onClose: () => dispatch(clearMessage()),
              });
              Swal.fire({
                title: "Information",
                icon: "success",
                text: "Data has been deleted successfully",
                showConfirmButton: false,
                timer: 2000,
              });
            })
            .catch((err) => {
              toast.error(this.props.message, {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                toastId: "customId-delete-product-failed",
                onClose: () => dispatch(clearMessage()),
              });
              this.setState({
                loading: false,
              });
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire("Cancelled", "Your data is safe :)", "error");
        }
      });
    } else {
      this.setState({
        loading: false,
      });
    }
  };
  handleSubmit = (e) => {
    const { dispatch, history } = this.props;
    if (this.state.collection) {
      Swal.fire({
        title: "Information",
        text: "Are you sure?",
        icon: "info",
        showCancelButton: true,
        confirmButtonText: "Yes",
      }).then((result) => {
        if (result.value) {
          dispatch(addProduct(this.state.collection))
            .then((response) => {
              window.$("#modal-product").modal("hide");
              toast.success("Data has been saved successfully", {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                toastId: "customId-add-product-success",
                onClose: () => dispatch(clearMessage()),
              });
              Swal.fire({
                title: "Information",
                icon: "success",
                text: "Data has been saved successfully",
                showConfirmButton: false,
                timer: 2000,
              }).then((result) => {});
            })
            .catch((err) => {
              toast.error(this.props.message, {
                position: "top-right",
                autoClose: 1500,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                toastId: "customId-add-product-failed",
                onClose: () => dispatch(clearMessage()),
              });
              this.setState({
                loading: false,
              });
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire("Cancelled", "Your data is safe :)", "error");
        }
      });
    } else {
      this.setState({
        loading: false,
      });
    }
  };
  render() {
    let {
      columns,
      defaultSorted,
      titleModal,
      showModal,
      bodyModal,
      displayModal,
      cellEditProps,
      product,
    } = this.state;
    return (
      <div className="sidebar-mini sidebar-collapse text-sm">
        <div className="wrapper">
          <ToastContainer />
          <Header />
          <SideMenu />
          <Modal
            id="modal-product"
            size="md"
            title={titleModal}
            show={showModal}
            body={bodyModal}
            onChangeValueParent={this.onChangeValueParent}
            handleSubmit={this.handleSubmit}
            display={displayModal}
          />
          <div className="content-wrapper">
            <div className="content-header">
              <div className="container-fluid">
                <div className="row mb-2">
                  <div className="col-sm-6"></div>
                  {/* /.col */}
                </div>
                {/* /.row */}
              </div>
              {/* /.container-fluid */}
            </div>

            <section className="content">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12 col-sm-12 col-12">
                    <div className="card card-info">
                      <div className="card-header ">
                        <h3 className="card-title">Product</h3>
                        <div className="card-tools float-right">
                          <a
                            href="#!"
                            type="button"
                            className="text-dark btn btn-sm  btn-warning"
                            data-toggle="modal"
                            data-target="#modal-product"
                          >
                            <i className="fas fa-plus-square" /> Product
                          </a>
                        </div>
                      </div>
                      <div className="card-body">
                        <RemoteTable
                          data={product}
                          columns={columns}
                          defaultSorted={defaultSorted}
                          cellEditProps={cellEditProps}
                        />
                      </div>
                    </div>
                  </div>
                  {/* /.col */}
                </div>
                {/* /.row */}
                {/* /.row */}
              </div>
              {/* /.container-fluid */}
            </section>
            {/* /.content */}
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { product } = state.product;
  const { message } = state.message;
  return {
    message,
    product,
  };
}

export default connect(mapStateToProps)(Product);
