import React, { Component } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import AllRoute from "./routes/AllRoute";
import Dashboard from "./views/Dashboard"; 
import Login from "./views/Login";
import { PrivateRoute } from './components/PrivateRoute';

import { connect } from "react-redux";

import { logout } from "./actions/auth";
import { clearMessage } from "./actions/message";

import { history } from './helpers/history';

class App extends Component {  
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);

    this.state = {
      currentUser: undefined
    };

    history.listen((location) => {
      props.dispatch(clearMessage()); // clear message when changing location
    });
  }
  componentDidMount() {
    const user = this.props.user;    
    if (user) {
      this.setState({
        currentUser: user
      });
    }
  }
  logOut() {
    this.props.dispatch(logout());
  }

  render() {
    const { menu } = this.props; 

    return (
      <Router history={history} basename={process.env.PUBLIC_URL}>
        <div> 
          <Switch>
            <PrivateRoute exact path={["/", "/home"]}  component={Dashboard} /> 
            <Route exact path="/login" component={Login} /> 
            <PrivateRoute  component={AllRoute} getMenu={menu}/> 
            <Redirect from="*" to="/" />
          </Switch>
        </div>
      </Router>
    );
  }
}
  
function mapStateToProps(state) {
  const { user } = state.auth;
  const { menu } = state.menu;
  return {
    user,
    menu
  };
}

export default connect(mapStateToProps)(App);